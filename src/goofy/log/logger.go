/*
Copyright 2016 George Trudeau

This file is part of goofy.

goofy is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

goofy is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with goofy. If not, see <http://www.gnu.org/licenses/>.
*/

package log

import (
	"fmt"
	"log"
	"os"
	"runtime"
)

func init() {
	Verbosity(TRACE)
}

/*
 * Logging level
 */

type Level string

const (
	OFF   Level = "OFF"
	FATAL Level = "FATAL"
	ERROR Level = "ERROR"
	WARN  Level = "WARN"
	INFO  Level = "INFO"
	DEBUG Level = "DEBUG"
	TRACE Level = "TRACE"
)

func (verbosity Level) String() string {
	return string(verbosity)
}

func (verbosity *Level) Set(value string) error {
	*verbosity = Level(value)
	return nil
}

func Verbosity(verbosity Level) {

	disable := disabled{}

	enable := enabled{
		log.New(os.Stdout, "", log.Ldate|log.Ltime|log.Lmicroseconds),
	}

	logger.Trace = disable
	logger.Debug = disable
	logger.Info = disable
	logger.Warn = disable
	logger.Error = disable
	logger.Fatal = disable

	// Enable specified level and below
	switch verbosity {
	case TRACE:
		logger.Trace = enable
		fallthrough
	case DEBUG:
		logger.Debug = enable
		fallthrough
	case INFO:
		logger.Info = enable
		fallthrough
	case WARN:
		logger.Warn = enable
		fallthrough
	case ERROR:
		logger.Error = enable
		fallthrough
	case FATAL:
		logger.Fatal = enable
	}
}

/*
 * Logging level implementations
 */

func Fatal(format string, values ...interface{}) {
	logger.Fatal.print("FATAL", format, values...)
}
func Error(format string, values ...interface{}) {
	logger.Error.print("ERROR", format, values...)
}
func Warn(format string, values ...interface{}) {
	logger.Warn.print("WARN", format, values...)
}
func Info(format string, values ...interface{}) {
	logger.Info.print("INFO", format, values...)
}
func Debug(format string, values ...interface{}) {
	logger.Debug.print("DEBUG", format, values...)
}
func Trace(format string, values ...interface{}) {
	logger.Trace.print("TRACE", format, values...)
}

/*
 * Logger internals
 */

var logger struct {
	Fatal printer
	Error printer
	Warn  printer
	Debug printer
	Info  printer
	Trace printer
}

type printer interface {
	print(level, format string, values ...interface{}) error
}

type enabled struct {
	*log.Logger
}

func (log enabled) print(level, format string, values ...interface{}) error {
	caller := "?"
	pc, _, _, ok := runtime.Caller(2)
	if ok {
		caller = runtime.FuncForPC(pc).Name()
	}
	format = string(
		append(
			[]byte("[%5s] %s === "),
			format...,
		),
	)
	values = append(
		[]interface{}{
			level,
			caller,
		},
		values...,
	)
	return log.Output(2, fmt.Sprintf(format, values...))
}

type disabled struct{}

func (_ disabled) print(_, _ string, _ ...interface{}) error {
	return nil
}
