/*
Copyright 2016 George Trudeau

This file is part of goofy.

goofy is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

goofy is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with goofy. If not, see <http://www.gnu.org/licenses/>.
*/

package service

import (
	"encoding/json"
	"goofy"
	"goofy/log"
	"goofy/worker/manager"
	"io/ioutil"
	"net"
	"net/rpc"
	"time"

	"github.com/hpcloud/tail"
)

const now = time.Second * 0

func Serve(rpcAddress, tcpAddress string, exit chan time.Duration) {

	err := manager.Security.Init()
	if err != nil {
		log.Fatal("SECURITY MANAGER > %s", err)
		exit <- now
	}

	err = manager.Task.Init()
	if err != nil {
		log.Fatal("TASK MANAGER > %s", err)
		exit <- now
	}

	go rpcs(rpcAddress, exit)
	go tcp(tcpAddress)
}

func rpcs(address string, exit chan time.Duration) {
	var err error

	err = rpc.Register(
		&Administration{
			exit,
		},
	)
	if err != nil {
		log.Fatal("ADMINISTRATION RPC > %s", err)
		exit <- now
	}

	err = rpc.Register(
		&Task{},
	)
	if err != nil {
		log.Fatal("TASK RPC > %s", err)
		exit <- now
	}

	listener, err := manager.Security.ListenTLS(address)
	if err != nil {
		log.Error("CANNOT START LISTENER > %s", err)
	}

	log.Info("LISTENING > %v", listener.Addr())
	for {
		conn, err := listener.Accept()
		if err != nil {
			log.Warn("LISTENER CANNOT ACCEPT CONNECTION > %s", err)
			continue
		}
		log.Info("NEW CONNECTION > %s", conn.RemoteAddr())
		go func(client net.Conn) {
			log.Debug("NEW RPC CONNECTION > %s", client.RemoteAddr())
			rpc.ServeConn(client)
			client.Close()
			log.Debug("RPC CONNECTION CLOSED > %s", client.RemoteAddr())
		}(conn)
	}
}

func tcp(address string) {
	listener, err := manager.Security.ListenTLS(address)
	if err != nil {
		log.Error("CANNOT START LISTENER > %s", err)
	}

	for {
		conn, err := listener.Accept()
		if err != nil {
			log.Warn("LISTENER CANNOT ACCEPT CONNECTION > %s", err)
			continue
		}
		go func(client net.Conn) {
			log.Debug("NEW RPC CONNECTION > %s", client.RemoteAddr())
			transmitLog(client)
			client.Close()
			log.Debug("RPC CONNECTION CLOSED > %s", client.RemoteAddr())
		}(conn)
	}
}
func transmitLog(client net.Conn) {
	//TODO: Allow to follow a task based on start time or maybe a run-ID
	//Currently, it will follow the first listed log for a given task on a given worker
	//Read goofy.FollowRequest
	data := make([]byte, 1024)
	n, err := client.Read(data)
	data = data[:n]

	log.Debug(string(data))
	var request goofy.FollowRequest
	err = json.Unmarshal(data, &request)
	if err != nil {
		log.Error("%s", err)
	}

	path := request.Taskname + "/"
	files, err := ioutil.ReadDir(path)
	if err != nil {
		log.Fatal("FATAL > %s", err)
		client.Close()
		return
	}

	if len(files) == 0 {
		log.Error("No file found for task %s", request.Taskname)
		client.Close()
		return
	}
	filename := files[len(files)-1].Name()

	log.Debug("%s", filename)

	t, err := tail.TailFile(path+filename, tail.Config{Follow: true})
	go func() {
		for line := range t.Lines {
			client.Write([]byte(line.Text + "\n"))
		}
	}()

	for !manager.Task.IsComplete(request.Taskname) {
	}
	t.Stop()
	client.Close()
	log.Trace("Transmission complete")
}
