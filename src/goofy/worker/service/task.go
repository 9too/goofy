/*
Copyright 2016 George Trudeau

This file is part of goofy.

goofy is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

goofy is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with goofy. If not, see <http://www.gnu.org/licenses/>.
*/

package service

import (
	"bufio"
	"goofy"
	"goofy/log"
	"goofy/worker/manager"
	"os"
	"os/exec"
)

type Task struct {
	taskComplete map[string]bool
}

func (work *Task) Run(task *goofy.Task, _ *bool) error {
	log.Debug("RUN TASK %s", task.Name)

	manager.Task.SetStatus(task.Name, false)

	cmd := exec.Command("/bin/bash", task.Command)

	cmdOut, err := cmd.StdoutPipe()
	if err != nil {
		log.Fatal("FATAL > %s", err)
	}

	go func() {
		err := os.MkdirAll(task.Name, 0755)
		if err != nil {
			log.Error("%s", err)
		}

		outfile := createLog(task.Name)
		err = cmd.Start()
		if err != nil {
			log.Error("ERROR start > %s", err)
		}

		scanner := bufio.NewScanner(cmdOut)
		go func() {
			for scanner.Scan() {
				_, err = outfile.WriteString(scanner.Text() + "\n")
				if err != nil {
					log.Error("%s", err)
				}
			}
		}()

		err = cmd.Wait()
		manager.Task.SetStatus(task.Name, true)
		if err != nil {
			log.Fatal("FATAL end > %s", err)
		}
		log.Trace("Task exec complete.")
	}()
	return nil
}

func createLog(taskname string) *os.File {
	err := os.MkdirAll(taskname, 0755)
	if err != nil {
		log.Error("%s", err)
	}

	outfile, err := os.Create(taskname + "/out.log")
	if err != nil {
		log.Fatal("FATAL > %s", err)
	}

	return outfile
}

func (work *Task) IsTaskComplete(taskname string, response *bool) error {
	*response = manager.Task.IsComplete(taskname)
	//log.Debug("Task %s complete = %s", task.Name, strconv.FormatBool(*response))
	return nil
}
