/*
Copyright 2016 George Trudeau

This file is part of goofy.

goofy is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

goofy is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with goofy. If not, see <http://www.gnu.org/licenses/>.
*/

package main

import (
	"flag"
	"goofy/log"
	"goofy/worker/manager"
	"goofy/worker/service"
	"net"
	"os"
	"os/signal"
	"syscall"
	"time"
)

var (
	address                string
	port                   string
	rpcAddress, tcpAddress string
	new                    bool
)

/*
 * Commands
 */

func start() {
	log.Info("WORKER STARTING")

	if manager.Certificate.LoadCA() != nil {
		return
	}

	if new {
		log.Debug("REQUESTING NEW CERTIFICATE...")
		err := manager.Certificate.Request(net.JoinHostPort(address, port))
		if err != nil {
			log.Warn("DID NOT GENERATE NEW CERTIFICATE > %v", err)
		} else {
			log.Info("NEW CERTIFICATE RECEIVED")
		}
	}

	if manager.Certificate.LoadCertificate() != nil {
		return
	}

	exit := make(chan time.Duration)
	go service.Serve(rpcAddress, tcpAddress, exit)

	// Wait for exit signal
	signals := make(chan os.Signal)
	signal.Notify(signals, syscall.SIGINT, syscall.SIGKILL, syscall.SIGHUP)
	select {
	case signal := <-signals:
		log.Debug("RECEIVED SIGNAL > %s", signal)
	case timeout := <-exit:
		log.Info("RECEIVED COMMAND TO STOP > shutting down in %v", timeout)
		time.Sleep(timeout)
	}
	log.Info("SHUTDOWN")
}

func startFlags() *flag.FlagSet {
	flags := flag.NewFlagSet("", flag.ExitOnError)
	flags.StringVar(&address, "address", "127.0.0.1", "server address")
	flags.StringVar(&port, "port", "1339", "server port")
	flags.StringVar(&rpcAddress, "rpc", ":1338", "RPC listening port")
	flags.StringVar(&tcpAddress, "tcp", ":4764", "TCP listening port")
	flags.BoolVar(&new, "new", false, "requests new certificate")
	return flags
}
