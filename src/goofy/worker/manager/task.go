/*
Copyright 2016 George Trudeau

This file is part of goofy.

goofy is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

goofy is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with goofy. If not, see <http://www.gnu.org/licenses/>.
*/

package manager

import (
	"goofy/log"
	"sync"
)

var Task = new(taskManager)

type taskManager struct {
	sync.RWMutex
	taskStatus map[string]bool
}

func (manager *taskManager) Init() error {
	manager.taskStatus = make(map[string]bool)
	return nil
}

func (manager *taskManager) IsComplete(taskname string) bool {
	manager.RLock()
	defer manager.RUnlock()
	return manager.taskStatus[taskname]
}

func (manager *taskManager) SetStatus(taskname string, status bool) {
	manager.Lock()
	defer manager.Unlock()
	manager.taskStatus[taskname] = status
	log.Debug("Task: %s complete = true", taskname)
}
