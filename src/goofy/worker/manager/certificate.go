/*
Copyright 2016 George Trudeau

This file is part of goofy.

goofy is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

goofy is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with goofy. If not, see <http://www.gnu.org/licenses/>.
*/

package manager

import (
	"crypto"
	"crypto/rand"
	"crypto/rsa"
	"crypto/tls"
	"crypto/x509"
	"crypto/x509/pkix"
	"encoding/pem"
	"goofy"
	"goofy/log"
	"io/ioutil"
	"net"
	"net/rpc"
	"os"
	"runtime"
)

var Certificate = new(certificateManager)

type certificateManager struct {
	worker tls.Certificate
	server *x509.CertPool
}

func (manager *certificateManager) LoadCA() error {
	pem, err := ioutil.ReadFile(goofy.CAPath)
	if err != nil {
		return err
	}
	manager.server = x509.NewCertPool()
	manager.server.AppendCertsFromPEM(pem)
	log.Debug("CERTIFICATE MANAGER INITIALIZED")
	return nil
}

func (manager *certificateManager) LoadCertificate() error {
	certificate, err := tls.LoadX509KeyPair(goofy.CertPath, goofy.KeyPath)
	if err != nil {
		log.Fatal("CANNOT LOAD CERTIFICATE > %s", err)
		return err
	}
	manager.worker = certificate
	return nil
}

func (manager *certificateManager) Request(address string) error {
	keypair, err := rsa.GenerateKey(rand.Reader, 2048)
	if err != nil {
		log.Error("CANNOT GENERATE RSA KEYPAIR > %s", err)
		return err
	}
	cert, err := manager.approve(keypair, address)
	if err != nil {
		log.Error("ERROR DURING REGISTRATION PROCESS > %s", err)
		return err
	}
	certOut, err := os.Create(goofy.CertPath)
	if err != nil {
		log.Fatal("FAILED TO OPEN (%s) > %s", goofy.CertPath, err)
		return err
	}
	pem.Encode(
		certOut,
		&pem.Block{
			Type:  "CERTIFICATE",
			Bytes: cert[0],
		},
	)
	pem.Encode(
		certOut,
		&pem.Block{
			Type:  "CERTIFICATE",
			Bytes: cert[1],
		},
	)
	certOut.Close()
	keyOut, err := os.OpenFile(
		goofy.KeyPath,
		os.O_WRONLY|os.O_CREATE|os.O_TRUNC,
		0600,
	)
	if err != nil {
		log.Fatal("FAILED TO OPEN (%s) > %s", goofy.KeyPath, err)
		return err
	}
	pem.Encode(
		keyOut,
		&pem.Block{
			Type:  "RSA PRIVATE KEY",
			Bytes: x509.MarshalPKCS1PrivateKey(keypair),
		},
	)
	keyOut.Close()
	return nil
}

func (manager *certificateManager) approve(signature crypto.Signer, address string) (chain [][]byte, err error) {
	conn, err := tls.Dial("tcp", address, &tls.Config{RootCAs: manager.server})
	if err != nil {
		log.Error("CANNOT DIAL SERVER > %s", err)
		return
	}
	defer conn.Close()
	localhost, _, err := net.SplitHostPort(conn.LocalAddr().String())
	if err != nil {
		log.Error("INVALID IP")
		return
	}
	hostname, err := os.Hostname()
	if err != nil {
		log.Warn("CANNOT RETRIEVE HOSTNAME")
	}
	csr, err := x509.CreateCertificateRequest(
		rand.Reader,
		&x509.CertificateRequest{
			Subject: pkix.Name{
				CommonName:         "worker",
				Organization:       []string{runtime.GOOS},
				OrganizationalUnit: []string{runtime.GOARCH},
			},
			IPAddresses: []net.IP{
				net.ParseIP(localhost),
			},
			DNSNames: []string{
				hostname,
			},
		},
		signature,
	)
	if err != nil {
		log.Error("CANNOT CREATE CERTIFICATION SIGNING REQUEST > %s", err)
		return
	}
	err = rpc.NewClient(conn).Call("Worker.Register", &csr, &chain)
	return
}
