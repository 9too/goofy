/*
Copyright 2016 George Trudeau

This file is part of goofy.

goofy is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

goofy is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with goofy. If not, see <http://www.gnu.org/licenses/>.
*/

package main

const (
	usage = `
goofy worker

Usage:

    worker command [arguments]

Commands :

    help          Print this help
    start         Start the worker

Use "worker help [command]" for more information about a command.

`
	startUsage = `
Start the worker

Usage:

    worker start [-address <IP>:PORT] [-listen <IP>:PORT] [-new]
                 [-verbosity LEVEL]

Arguments :

    -address      Server address (default: :1338)
    -listen       RPC port (default: :1339)
    -new          New worker, generate certificate (default: false)
    -verbosity    Log verbosity {OFF,FATAL,ERROR,WARN,INFO,DEBUG,TRACE}

`
)
