/*
Copyright 2016 George Trudeau

This file is part of goofy.

goofy is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

goofy is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with goofy. If not, see <http://www.gnu.org/licenses/>.
*/

package goofy

import (
	"flag"
	"fmt"
	"os"
)

/*
 * Command
 */

type Command struct {
	Call  func()
	Flags *flag.FlagSet
	Usage string
}

func (command *Command) Parse() {
	command.Flags.Parse(os.Args[2:])
}

type Commands map[string]Command

/*
 * CommandLine
 */

type CommandLine struct {
	Usage    string
	Commands Commands
}

func (cli *CommandLine) help(command string) {
	c, ok := cli.Commands[command]
	if ok {
		fmt.Print(c.Usage)
	} else {
		invalid(command)
	}
	os.Exit(0)
}

func (cli *CommandLine) Parse() Command {
	if len(os.Args) <= 1 {
		fmt.Print(cli.Usage)
		os.Exit(1)
	}
	if os.Args[1] == "help" {
		if len(os.Args) <= 2 {
			fmt.Print(cli.Usage)
		} else {
			cli.help(os.Args[2])
		}
	}
	command, ok := cli.Commands[os.Args[1]]
	if !ok {
		invalid(os.Args[1])
	}
	return command
}

func invalid(command string) {
	fmt.Fprintln(os.Stderr, "Invalid command:", command)
	os.Exit(2)
}
