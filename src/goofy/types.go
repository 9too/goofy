/*
Copyright 2016 George Trudeau

This file is part of goofy.

goofy is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

goofy is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with goofy. If not, see <http://www.gnu.org/licenses/>.
*/

package goofy

import (
	"crypto/x509"
	"encoding/json"
)

/*
 * Task
 */

type Task struct {
	Name        string
	Command     string
	Description string
}

type Trigger struct {
	Task   string
	Signal string
	Target Platform
}

func (task *Task) Marshal() ([]byte, error) {
	return json.MarshalIndent(task, "", "  ")
}

func (task *Task) Unmarshal(in []byte) error {
	return json.Unmarshal(in, task)
}

func (task *Task) String() string {
	json, _ := task.Marshal()
	return string(json)
}

type Schedule struct {
	Minute, Hour, Day, Month int8
}

type Event struct {
	Type    string
	channel chan bool
}

type Group string

type Parameter struct {
	Description, Type string
	Required          bool
}

type DispatchRequest struct {
	Workers []int64
	Task    string
}

type FollowRequest struct {
	WorkerID int64
	Taskname string
}

/*
 * Worker
 */

type Worker struct {
	ID          int64
	IP          string
	Certificate *x509.Certificate
	Platform
}

type Platform struct {
	Hostname, OS, Arch string
}

func (platform Platform) Match(filter Platform) bool {
	var any string
	return (filter.OS == any || filter.OS == platform.OS) &&
		(filter.Arch == any || filter.Arch == platform.Arch) &&
		(filter.Hostname == any || filter.Hostname == platform.Hostname)
}
