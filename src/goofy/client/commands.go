/*
Copyright 2016 George Trudeau

This file is part of goofy.

goofy is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

goofy is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with goofy. If not, see <http://www.gnu.org/licenses/>.
*/

package main

import (
	"bufio"
	"crypto/tls"
	"crypto/x509"
	"encoding/json"
	"flag"
	"fmt"
	"goofy"
	"io/ioutil"
	"net"
	"net/rpc"
	"os"
	"path/filepath"
	"time"
)

/*
 * Flags
 */

var (
	address, port string
	path          string
	taskname      string
	script        string
	timeout       uint
)

/*
 * Commands & Flagset
 */
func create() {
	json, err := ioutil.ReadFile(path)
	fatal(err)

	task := new(goofy.Task)
	err = task.Unmarshal(json)
	fatal(err)

	conn := dialServer()
	defer conn.Close()
	err = rpc.NewClient(conn).Call("Task.New", task, new(bool))
	fatal(err)
}

func createFlags() *flag.FlagSet {
	flags := flag.NewFlagSet("create", flag.ExitOnError)
	flags.StringVar(&path, "json", "", "")
	return flags
}

func run() {
	conn := dialServer()
	defer conn.Close()
	client := rpc.NewClient(conn)
	defer client.Close()

	done := false
	address := "4763"
	workerId := int64(1)
	go printOutput(address, workerId, &done)

	workers := make([]int64, 1)
	workers[0] = workerId
	request := &goofy.DispatchRequest{
		Task:    taskname,
		Workers: workers,
	}
	err := client.Call("Task.Dispatch", request, new(bool))
	fatal(err)

	for !done {
	}
}

func printOutput(serverPort string, workerId int64, done *bool) {
	conn := dialServerAddr(serverPort)
	request := goofy.FollowRequest{Taskname: taskname, WorkerID: workerId}
	data, err := json.Marshal(request)
	_, err = conn.Write(data)
	fatal(err)

	scanner := bufio.NewScanner(conn)
	for scanner.Scan() {
		fmt.Println(scanner.Text())
	}
	*done = true
}

func runFlags() *flag.FlagSet {
	flags := flag.NewFlagSet("run", flag.ExitOnError)
	flags.StringVar(&taskname, "name", "", "")
	return flags
}

func list() {
	conn := dialServer()
	defer conn.Close()
	tasks := new([]*goofy.Task)
	err := rpc.NewClient(conn).Call("Task.ListTasks", tasks, tasks)
	fatal(err)

	for _, task := range *tasks {
		fmt.Println(task)
	}
}

func listFlags() *flag.FlagSet {
	flags := flag.NewFlagSet("list", flag.ExitOnError)
	return flags
}

func stop() {
	conn := dialServer()
	defer conn.Close()
	timeout := time.Duration(timeout) * time.Second
	var ok bool
	err := rpc.NewClient(conn).Call("Administration.Stop", &timeout, &ok)
	fatal(err)

	if ok {
		fmt.Println("server will shutdown in", timeout)
	}
}

func stopFlags() *flag.FlagSet {
	flags := flag.NewFlagSet("stop", flag.ExitOnError)
	flags.UintVar(&timeout, "timeout", 10, "")
	return flags
}

/*
 * Utilities
 */

func dialServer() net.Conn {
	caFile, err := os.Open(
		filepath.Join(
			filepath.Dir(os.Args[0]),
			goofy.CAPath,
		),
	)
	fatal(err)

	caPEM, err := ioutil.ReadAll(caFile)
	fatal(err)

	ca := x509.NewCertPool()
	ca.AppendCertsFromPEM(caPEM)
	conn, err := tls.Dial(
		"tcp",
		net.JoinHostPort(address, port),
		&tls.Config{RootCAs: ca},
	)
	fatal(err)

	return conn
}

func dialServerAddr(port string) net.Conn {
	caFile, err := os.Open(
		filepath.Join(
			filepath.Dir(os.Args[0]),
			goofy.CAPath,
		),
	)
	fatal(err)

	caPEM, err := ioutil.ReadAll(caFile)
	fatal(err)

	ca := x509.NewCertPool()
	ca.AppendCertsFromPEM(caPEM)
	conn, err := tls.Dial(
		"tcp",
		net.JoinHostPort(address, port),
		&tls.Config{RootCAs: ca},
	)
	fatal(err)

	return conn
}

func fatal(err error) {
	if err != nil {
		fmt.Printf("FATAL > %s\n", err)
		os.Exit(2)
	}
}
