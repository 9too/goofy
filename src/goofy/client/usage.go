/*
Copyright 2016 George Trudeau

This file is part of goofy.

goofy is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

goofy is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with goofy. If not, see <http://www.gnu.org/licenses/>.
*/

package main

const (
	usage = `
simple cli to interact with the goofy server

Usage:

    cli command [-address ADDRESS] [-port PORT]
                [arguments...]

Commands :

    create    Create a new job
    help      Print this help
    list      Lists available tasks
    stop      Stop the server
    run       Run a task

Options :

    -address    server address (default: 127.0.0.1)
    -port       server port (default: 1339)

Use "cli help [command]" for more information about a command.

`
	createUsage = `
Create a new task and submit it to the goofy server

Usage:

    cli create [-address ADDRESS] [-port PORT]
               [-json PATH]

Arguments :

    -json    path to a json task file

`
	listUsage = `
List all the available tasks on the goofy server

Usage:

    cli list [-address ADDRESS] [-port PORT]
`
	stopUsage = `
Stop the remote goofy server

Usage:

    cli stop [-address ADDRESS] [-port PORT]
             [-timeout TIMEOUT]

Arguments :

    -timeout    server shutdown timeout in seconds (default: 10)

`

	runUsage = `
Run an existing task on the server

Usage:

    cli run -file 'taskName'

Arguments :

    file    Points to the remote file which the server will run

`
)
