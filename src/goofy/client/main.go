/*
Copyright 2016 George Trudeau

This file is part of goofy.

goofy is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

goofy is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with goofy. If not, see <http://www.gnu.org/licenses/>.
*/

package main

import (
	"goofy"
)

var version string

func main() {
	line := &goofy.CommandLine{
		usage,
		goofy.Commands{
			"create": {
				Call:  create,
				Flags: createFlags(),
				Usage: createUsage,
			},
			"list": {
				Call:  list,
				Flags: listFlags(),
				Usage: listUsage,
			},
			"run": {
				Call:  run,
				Flags: runFlags(),
				Usage: runUsage,
			},
			"stop": {
				Call:  stop,
				Flags: stopFlags(),
				Usage: stopUsage,
			},
		}}
	command := line.Parse()
	command.Flags.StringVar(&address, "address", "127.0.0.1", "")
	command.Flags.StringVar(&port, "port", "1339", "")
	command.Parse()
	command.Call()
}
