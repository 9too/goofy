/*
Copyright 2016 George Trudeau

This file is part of goofy.

goofy is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

goofy is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with goofy. If not, see <http://www.gnu.org/licenses/>.
*/

package main

import (
	"flag"
	"goofy/log"
	"goofy/server/service"
	"os"
	"os/signal"
	"syscall"
	"time"
)

var (
	rpcAddress  string
	httpAddress string
	tcpAddress  string
)

func start() {
	log.Info("SERVER STARTING")
	exit := make(chan time.Duration)
	signals := make(chan os.Signal)
	signal.Notify(signals, syscall.SIGINT, syscall.SIGKILL, syscall.SIGHUP)
	go service.Serve(tcpAddress, rpcAddress, httpAddress, exit)
	select {
	case signal := <-signals:
		log.Debug("RECEIVED SIGNAL > %s", signal)
	case timeout := <-exit:
		log.Info("SHUTTING DOWN IN > %s", timeout)
		time.Sleep(timeout)
	}
	// TODO : Shutdown all workers ?
	log.Info("SHUTDOWN")
}

func startFlags() *flag.FlagSet {
	flags := flag.NewFlagSet("start", flag.ExitOnError)
	flags.StringVar(&rpcAddress, "rpc", ":1339", "")
	flags.StringVar(&httpAddress, "http", ":8080", "")
	flags.StringVar(&tcpAddress, "tcp", ":4763", "")
	return flags
}
