/*
Copyright 2016 George Trudeau

This file is part of goofy.

goofy is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

goofy is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with goofy. If not, see <http://www.gnu.org/licenses/>.
*/

package service

import (
	"goofy"
	"goofy/log"
	"goofy/server/manager"
)

type Worker struct{}

func (_ *Worker) List(filter goofy.Platform, list *[]goofy.Worker) error {
	*list = manager.Worker.List()
	return nil
}

func (_ *Worker) Register(csr *[]byte, chain *[][]byte) error {
	request, err := manager.ParseCSR(csr)
	if err != nil {
		return err
	}
	worker := manager.Worker.New()
	*chain, err = manager.Certificate.CertifyWorker(&worker, request)
	if err != nil {
		return err
	}
	err = manager.Worker.Register(worker)
	if err != nil {
		return err
	}
	err = manager.SaveCertificate(worker.ID, chain)
	if err != nil {
		return err
	}
	log.Info("NEW WORKER CERTIFICATE REGISTERED")
	return nil
}
