/*
Copyright 2016 George Trudeau

This file is part of goofy.

goofy is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

goofy is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with goofy. If not, see <http://www.gnu.org/licenses/>.
*/

package service

import (
	"bufio"
	"encoding/json"
	"errors"
	"goofy"
	"goofy/log"
	"goofy/server/manager"
	"net"
	"net/rpc"
	"os"
	"strconv"
	"time"
)

type Task struct{}

func (_ *Task) Dispatch(request goofy.DispatchRequest, reply *bool) error {
	log.Info("DISPATCHING TASK '%s' ON WORKERS %v", request.Task, request.Workers)
	*reply = true

	workers, err := manager.Worker.Get(request.Workers...)
	if err != nil {
		log.Fatal("FAILED TO RETREIVE WORKERS %v", request.Workers)
		return err
	}
	task := manager.Task.Get(request.Task)
	if task == nil {
		err := errors.New("Invalid task")
		log.Error("INVALID TASK REQUEST '%s'", task.Name)
		return err
	}

	go func() {
		for _, worker := range workers {
			path := strconv.Itoa(int(worker.ID)) + "/" + request.Task + "/"
			os.MkdirAll(path, 0755)
			filename := path + time.Now().String() + ".log"
			outfile, err := os.Create(filename)
			if err != nil {
				log.Error("%s", err)
			}
			conn, err := manager.Security.DialTLS(
				net.JoinHostPort(worker.IP, "1338"),
			)
			if err != nil {
				log.Error("CANNOT DIAL WORKER (%s) > %s", worker.Hostname, err)
				continue
			}
			err = rpc.NewClient(conn).Call("Task.Run", task, new(bool))
			if err != nil {
				log.Error("ERROR DURING WORKER (%s) DISPATCH '%s' > %s", worker.Hostname, task.Name, err)
				continue
			}
			log.Trace("Returned from dispatch call")

			workerOut, err := manager.Security.DialTLS(
				net.JoinHostPort(worker.IP, "4764"),
			)
			if err != nil {
				log.Error("%s", err)
			}

			request := goofy.FollowRequest{Taskname: task.Name, WorkerID: worker.ID}
			data, err := json.Marshal(request)
			_, err = workerOut.Write(data)
			if err != nil {
				log.Error("%s", err)
			}

			scanner := bufio.NewScanner(workerOut)
			for scanner.Scan() {
				outfile.WriteString(scanner.Text() + "\n")
			}
			outfile.Close()
		}
	}()

	return nil
}

func (_ *Task) Delete(name string, reply *bool) error {
	log.Debug("DELETE TASK REQUEST > '%s'", name)
	*reply = true
	return manager.Task.Delete(name)
}

func (_ *Task) Modify(task goofy.Task, reply *bool) error {
	log.Debug("MODIFY TASK REQUEST > '%s'", task.Name)
	*reply = true
	return manager.Task.Modify(&task)
}

func (_ *Task) New(task goofy.Task, reply *bool) error {
	log.Debug("NEW TASK REQUEST > '%s'", task.Name)
	*reply = true
	return manager.Task.New(&task)
}

func (_ *Task) List(_, list *[]*goofy.Task) error {
	log.Debug("LISTING TASKS")
	*list = manager.Task.List()
	return nil
}

func check(err error) {
	if err != nil {
		log.Error("%s", err)
	}
}
