/*
Copyright 2016 George Trudeau

This file is part of goofy.

goofy is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

goofy is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with goofy. If not, see <http://www.gnu.org/licenses/>.
*/

package service

import (
	"goofy"
	"goofy/log"
	"goofy/server/manager"
	"net"
	"net/http"
	"net/rpc"
	"net/rpc/jsonrpc"
	"time"

	"golang.org/x/net/websocket"
)

const now = time.Second * 0

func Serve(tcpAddress string, rpcAddress, httpAddress string, exit chan time.Duration) {
	var err error

	err = manager.Certificate.Init()
	if err != nil {
		log.Fatal("CERTIFICATE MANAGER > %s", err)
		exit <- now
	}

	err = manager.Security.Init()
	if err != nil {
		log.Fatal("SECURITY MANAGER > %s", err)
		exit <- now
	}

	err = manager.Task.Init()
	if err != nil {
		log.Fatal("TASK MANAGER > %s", err)
		exit <- now
	}

	err = manager.Worker.Init()
	if err != nil {
		log.Fatal("WORKER MANAGER > %s", err)
		exit <- now
	}

	go rpcs(rpcAddress, exit)
	go https(httpAddress)
	go tcp(tcpAddress)
}

func rpcs(address string, exit chan time.Duration) {
	/*
	   TODO : Split RPC access

	   Administration : Admin rights
	   Tasking        : Clients
	   Registration   : Workers
	*/
	var err error

	err = rpc.Register(&Administration{exit})
	if err != nil {
		log.Fatal("ADMINISTRATION RPC > %s", err)
		exit <- now
	}

	err = rpc.Register(new(Task))
	if err != nil {
		log.Fatal("TASK RPC > %s", err)
		exit <- now
	}

	err = rpc.Register(new(Worker))
	if err != nil {
		log.Fatal("WORKER RPC > %s", err)
		exit <- now
	}

	listener, err := manager.Security.ListenTLS(address)
	if err != nil {
		log.Fatal("CANNOT START RPC > %s", err)
	} else {
		log.Info("LISTENING > %v", listener.Addr())
	}

	for {
		conn, err := listener.Accept()
		if err != nil {
			log.Warn("LISTENER CANNOT ACCEPT CONNECTION > %s", err)
			continue
		}
		go func(client net.Conn) {
			log.Debug("NEW RPC CONNECTION > %s", client.RemoteAddr())
			rpc.ServeConn(client)
			client.Close()
			log.Debug("RPC CONNECTION CLOSED > %s", client.RemoteAddr())
		}(conn)
	}
}

func https(address string) {
	http.Handle(
		"/",
		http.FileServer(
			http.Dir(goofy.WebDirectory),
		),
	)
	http.Handle(
		"/ws",
		websocket.Server{
			Config: websocket.Config{TlsConfig: manager.Security.TlsConfig},
			Handler: func(ws *websocket.Conn) {
				log.Debug("NEW WEBSOCKET CONNECTION")
				jsonrpc.ServeConn(ws)
			},
		},
	)
	http.Handle(
		"/out",
		websocket.Server{
			Config: websocket.Config{TlsConfig: manager.Security.TlsConfig},
			Handler: func(ws *websocket.Conn) {
				log.Trace("NEW WEBSOCKET CONNECTION (out)")
				manager.Task.Follow(ws)
			},
		},
	)
	http.ListenAndServeTLS(address, goofy.CertPath, goofy.KeyPath, nil)
}

func tcp(address string) {
	listener, err := manager.Security.ListenTLS(address)
	check(err)
	for {
		conn, err := listener.Accept()
		if err != nil {
			log.Warn("LISTENER CANNOT ACCEPT CONNECTION > %s", err)
			continue
		}
		go func(client net.Conn) {
			log.Debug("NEW RPC CONNECTION > %s", client.RemoteAddr())
			manager.Task.Follow(client)
			client.Close()
			log.Debug("RPC CONNECTION CLOSED > %s", client.RemoteAddr())
		}(conn)
	}
}

func outputHandler(ws *websocket.Conn) {
	manager.Task.Follow(ws)
}

/*
	msg := make([]byte, 1024)
	n, err := ws.Read(msg)
	if err != nil {
		log.Fatal("FATAL > %s", err)
	}
	msg = msg[0:n]

	log.Debug("%s", msg)

	var request goofy.FollowRequest
	json.Unmarshal(msg, &request)

	log.Debug("WorkerID: %s, Taskname: %s", request.WorkerID, request.Taskname)

	wrkDirectory := strconv.Itoa(int(request.WorkerID)) + "/" + request.Taskname + "/"
	files := make([]os.FileInfo, 0)
	for len(files) <= 0 {
		files, err = ioutil.ReadDir(wrkDirectory)
		if err != nil {
			log.Fatal("FATAL > %s", err)
			ws.Close()
			return
		}
	}
	filename := files[len(files)-1].Name()

	worker, err := manager.Worker.Get(request.WorkerID)
	if err != nil {
		log.Error("%s", err)
	}
	conn, err := manager.Security.DialTLS(
		net.JoinHostPort(worker[0].IP, "1338"),
	)
	if err != nil {
		log.Error("%s", err)
	}
	workerClient := rpc.NewClient(conn)

	t, err := tail.TailFile(wrkDirectory+filename, tail.Config{Follow: true})
	if err != nil {
		log.Warn("WARN > %s", err)
	}

	ws.Write([]byte("Initializing output...\n"))
	for line := range t.Lines {
		ws.Write([]byte(line.Text + "\n"))
	}

	taskComplete := false
	for !taskComplete {
		err := workerClient.Call("Task.IsTaskComplete", request.Taskname, &taskComplete)
		if err != nil {
			log.Error("%s", err)
		}
	}

	log.Trace("Task completed, transmission to client done.")

	t.Stop()
	ws.Close()

}*/
