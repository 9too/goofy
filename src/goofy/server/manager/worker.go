/*
Copyright 2016 George Trudeau

This file is part of goofy.

goofy is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

goofy is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with goofy. If not, see <http://www.gnu.org/licenses/>.
*/

package manager

import (
	"crypto/x509"
	"encoding/pem"
	"errors"
	"fmt"
	"goofy"
	"goofy/log"
	"io/ioutil"
	"os"
	"path/filepath"
	"strconv"
	"sync"
)

var Worker = new(workerManager)

type workerManager struct {
	sync.RWMutex
	next    int64
	workers map[int64]goofy.Worker
}

func (manager *workerManager) Init() error {
	var err error
	manager.next = 1
	manager.workers = make(map[int64]goofy.Worker)
	err = os.MkdirAll(goofy.WorkersDirectory, 0755)
	if err != nil {
		log.Fatal("CANNOT CREATE WORKERS DIRECTORY > %s", err)
		return err
	}
	err = manager.Load()
	if err != nil {
		log.Fatal("CANNOT LOAD WORKERS > %s", err)
		return err
	}
	log.Debug("WORKER MANAGER INITIALIZED")
	return nil
}

func (manager *workerManager) Filter(filter goofy.Platform) []goofy.Worker {
	all := manager.List()
	list := all[:0]
	for _, worker := range all {
		if worker.Platform.Match(filter) {
			list = append(list, worker)
		}
	}
	return list
}

func (manager *workerManager) Get(ids ...int64) ([]goofy.Worker, error) {
	// Read Lock
	manager.RLock()
	log.Debug("WORKERS READ LOCK (+)")
	defer log.Debug("WORKERS READ LOCK (-)")
	defer manager.RUnlock()
	//
	workers := make([]goofy.Worker, 0)
	for _, id := range ids {
		worker, ok := manager.workers[id]
		if !ok {
			return nil, errors.New(fmt.Sprintf("Bad worker id #%v", id))
		}
		workers = append(workers, worker)
	}
	return workers, nil
}

func (manager *workerManager) List() []goofy.Worker {
	// Read Lock
	manager.RLock()
	log.Debug("WORKERS READ LOCK (+)")
	defer log.Debug("WORKERS READ LOCK (-)")
	defer manager.RUnlock()
	//
	list := make([]goofy.Worker, 0, len(manager.workers))
	for _, worker := range manager.workers {
		list = append(list, worker)
	}
	return list
}

func (manager *workerManager) New() goofy.Worker {
	// Write Lock
	manager.Lock()
	log.Debug("WORKERS WRITE LOCK (+)")
	defer log.Debug("WORKERS WRITE LOCK (-)")
	defer manager.Unlock()
	//
	worker := goofy.Worker{
		ID: manager.next,
	}
	manager.next++
	return worker
}

func (manager *workerManager) Register(worker goofy.Worker) error {
	// Write Lock
	manager.Lock()
	log.Debug("WORKERS WRITE LOCK (+)")
	defer log.Debug("WORKERS WRITE LOCK (-)")
	defer manager.Unlock()
	//
	if worker.Certificate == nil {
		log.Error("WORKER NOT CERTIFIED")
		return errors.New("uncertified worker")
	}
	for _, registree := range manager.workers {
		if worker.ID == registree.ID {
			log.Warn("WORKER ID ALREADY REGISTERED > %v", worker.ID)
			return errors.New("worker id already registered")
		} else if worker.IP == registree.IP {
			log.Warn("WORKER IP ALREADY REGISTERED > %v", worker.IP)
			return errors.New("worker ip already registered")
		}
	}
	manager.workers[worker.ID] = worker
	os.MkdirAll(strconv.Itoa(int(worker.ID)), 0755)
	return nil
}

func (manager *workerManager) Load() error {
	// Write Lock
	manager.Lock()
	log.Debug("WORKERS WRITE LOCK (+)")
	defer log.Debug("WORKERS WRITE LOCK (-)")
	defer manager.Unlock()
	//
	log.Info("LOADING SAVED WORKERS")
	return filepath.Walk(
		goofy.WorkersDirectory,
		func(path string, info os.FileInfo, err error) error {
			switch {
			case info.IsDir():
				log.Trace("ENTERING DIRECTORY (%s)", path)
			case err != nil:
				log.Error("ERROR DURING WALK TO %s > %s", path, err)
			default:
				pemFile, err := ioutil.ReadFile(path)
				if err != nil {
					log.Error("CANNOT READ WORKER'S CERTIFICATE (%s) > %s", path, err)
					return nil
				}
				der, _ := pem.Decode(pemFile)
				cert, err := x509.ParseCertificate(der.Bytes)
				if err != nil {
					log.Error("ERROR PARSING WORKER'S CERTIFICATE (%s) > %s", path, err)
					return nil
				}
				worker := goofy.Worker{
					Platform:    ReadPlatform(cert),
					ID:          cert.SerialNumber.Int64(),
					IP:          cert.IPAddresses[0].String(),
					Certificate: cert,
				}
				manager.workers[worker.ID] = worker
				manager.next = manager.next + 1
				log.Info("WORKER #%v ADDED TO WORKERS (IP: %v)", worker.ID, worker.IP)
			}
			return nil
		})
}
