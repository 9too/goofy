/*
Copyright 2016 George Trudeau

This file is part of goofy.

goofy is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

goofy is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with goofy. If not, see <http://www.gnu.org/licenses/>.
*/

package manager

import (
	"crypto/rand"
	"crypto/tls"
	"crypto/x509"
	"encoding/pem"
	"fmt"
	"goofy"
	"goofy/log"
	"math/big"
	"os"
	"strconv"
	"time"
)

var Certificate = new(certificateManager)

type certificateManager struct {
	ca   tls.Certificate
	pool *x509.CertPool
}

func (manager *certificateManager) Init() error {
	var err error
	manager.ca, err = tls.LoadX509KeyPair(goofy.CertPath, goofy.KeyPath)
	if err != nil {
		return err
	}
	manager.ca.Leaf, err = x509.ParseCertificate(manager.ca.Certificate[0])
	if err != nil {
		return err
	}
	manager.pool = x509.NewCertPool()
	manager.pool.AddCert(manager.ca.Leaf)
	log.Debug("CERTIFICATE MANAGER INITIALIZED")
	return nil
}

func (manager *certificateManager) CertifyWorker(worker *goofy.Worker, csr *x509.CertificateRequest) (chain [][]byte, err error) {
	log.Info("CERTIFICATE REQUEST FROM > %s", csr.IPAddresses)
	const DayYear = 24 * 365
	notBefore := time.Now()
	notAfter := notBefore.Add(time.Hour * DayYear)
	der, err := x509.CreateCertificate(
		rand.Reader,
		&x509.Certificate{
			SerialNumber:          big.NewInt(worker.ID),
			Subject:               csr.Subject,
			IPAddresses:           csr.IPAddresses,
			DNSNames:              csr.DNSNames,
			NotBefore:             notBefore,
			NotAfter:              notAfter,
			KeyUsage:              x509.KeyUsageKeyEncipherment,
			BasicConstraintsValid: true,
		},
		manager.ca.Leaf,
		csr.PublicKey, manager.ca.PrivateKey,
	)
	if err != nil {
		return
	}
	worker.Certificate, err = x509.ParseCertificate(der)
	if err != nil {
		return
	}
	worker.IP = worker.Certificate.IPAddresses[0].String()
	worker.Platform = ReadPlatform(worker.Certificate)
	chain = [][]byte{der, manager.ca.Certificate[0]}
	return
}

func ParseCSR(csr *[]byte) (*x509.CertificateRequest, error) {
	request, err := x509.ParseCertificateRequest(*csr)
	if err != nil {
		return nil, err
	}
	err = request.CheckSignature()
	if err != nil {
		return nil, err
	}
	return request, nil
}

func SaveCertificate(id int64, der *[][]byte) error {
	file, err := os.OpenFile(
		fmt.Sprint(
			goofy.WorkersDirectory, strconv.FormatInt(id, 10), ".pem",
		),
		os.O_APPEND|os.O_CREATE|os.O_WRONLY,
		0644,
	)
	defer file.Close()
	if err != nil {
		return err
	}
	err = pem.Encode(
		file,
		&pem.Block{
			Type:  "CERTIFICATE",
			Bytes: (*der)[0],
		},
	)
	if err != nil {
		return err
	}
	return nil
}

func ReadPlatform(certificate *x509.Certificate) goofy.Platform {
	var hostname, os, arch string
	if len(certificate.DNSNames) > 0 {
		hostname = certificate.DNSNames[0]
	}
	if len(certificate.Subject.Organization) > 0 {
		os = certificate.Subject.Organization[0]
	}
	if len(certificate.Subject.OrganizationalUnit) > 0 {
		arch = certificate.Subject.OrganizationalUnit[0]
	}
	return goofy.Platform{
		Arch:     arch,
		OS:       os,
		Hostname: hostname,
	}
}
