/*
Copyright 2016 George Trudeau

This file is part of goofy.

goofy is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

goofy is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with goofy. If not, see <http://www.gnu.org/licenses/>.
*/

package manager

import (
	"crypto/tls"
	"goofy/log"
	"net"
)

var Security = new(securityManager)

type securityManager struct {
	TlsConfig *tls.Config
}

func (manager *securityManager) Init() error {
	manager.TlsConfig = &tls.Config{
		Certificates: []tls.Certificate{Certificate.ca},
		RootCAs:      Certificate.pool,
	}
	log.Debug("SECURITY MANAGER INITIALIZED")
	return nil
}

func (manager *securityManager) DialTLS(address string) (net.Conn, error) {
	return tls.Dial("tcp", address, manager.TlsConfig)
}

func (manager *securityManager) ListenTLS(address string) (net.Listener, error) {
	return tls.Listen("tcp", address, manager.TlsConfig)
}
