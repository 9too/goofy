/*
Copyright 2016 George Trudeau

This file is part of goofy.

goofy is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

goofy is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with goofy. If not, see <http://www.gnu.org/licenses/>.
*/

package manager

import (
	"encoding/json"
	"errors"
	"fmt"
	"goofy"
	"goofy/log"
	"io/ioutil"
	"net"
	"net/rpc"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	"sync"

	"github.com/hpcloud/tail"
)

var Task = new(taskManager)

type taskManager struct {
	sync.RWMutex
	tasks map[string]*goofy.Task
}

func (manager *taskManager) Init() error {
	var err error
	manager.tasks = make(map[string]*goofy.Task)
	err = os.MkdirAll("tasks", 0755)
	if err != nil {
		log.Fatal("CANNOT CREATE TASKS DIRECTORY > %s", err)
		return err
	}
	err = manager.Load()
	if err != nil {
		log.Fatal("CANNOT LOAD TASKS > %s", err)
		return err
	}
	log.Debug("TASK MANAGER INITIALIZED")
	return nil
}

func (manager *taskManager) Delete(name string) error {
	// Write Lock
	manager.Lock()
	log.Debug("TASKS WRITE LOCK (+)")
	defer log.Debug("TASKS WRITE LOCK (-)")
	defer manager.Unlock()
	//
	_, exists := manager.tasks[name]
	if !exists {
		return errors.New("task doesn't exist")
	}
	err := os.RemoveAll(
		fmt.Sprint(
			goofy.TasksDirectory+strings.ToLower(name), name, ".json",
		),
	)
	if err != nil {
		return err
	}
	delete(manager.tasks, name)
	return nil
}

func (manager *taskManager) Get(name string) *goofy.Task {
	// Read Lock
	manager.RLock()
	log.Debug("TASKS READ LOCK (+)")
	defer log.Debug("TASKS READ LOCK (-)")
	defer manager.RUnlock()
	//
	return manager.tasks[name]
}

func (manager *taskManager) List() (list []*goofy.Task) {
	// Read Lock
	manager.RLock()
	log.Debug("TASKS READ LOCK (+)")
	defer log.Debug("TASKS READ LOCK (-)")
	defer manager.RUnlock()
	//
	for _, task := range manager.tasks {
		list = append(list, task)
	}
	return
}

func (manager *taskManager) Modify(task *goofy.Task) error {
	// Write Lock
	manager.Lock()
	log.Debug("TASKS WRITE LOCK (+)")
	defer log.Debug("TASKS WRITE LOCK (-)")
	defer manager.Unlock()
	//
	_, exists := manager.tasks[task.Name]
	if !exists {
		return errors.New("task doesn't exist")
	}
	file, err := os.OpenFile(
		fmt.Sprint(
			goofy.TasksDirectory+strings.ToLower(task.Name), task.Name, ".json",
		),
		os.O_WRONLY,
		0644,
	)
	defer file.Close()
	if err != nil {
		return err
	}
	json, err := task.Marshal()
	if err != nil {
		return err
	}
	err = file.Truncate(0)
	if err != nil {
		return err
	}
	_, err = file.Write(json)
	if err != nil {
		return err
	}
	manager.tasks[task.Name] = task
	return nil
}

func (manager *taskManager) New(task *goofy.Task) error {
	// Write Lock
	manager.Lock()
	log.Debug("TASKS WRITE LOCK (+)")
	defer log.Debug("TASKS WRITE LOCK (-)")
	defer manager.Unlock()
	//
	_, exists := manager.tasks[task.Name]
	if exists {
		return errors.New("Task already exists")
	}
	err := os.MkdirAll(goofy.TasksDirectory+task.Name+"/out/", os.ModePerm)
	if err != nil {
		return err
	}

	file, err := os.Create(
		fmt.Sprint(
			goofy.TasksDirectory+task.Name+"/", task.Name, ".json",
		),
	)
	defer file.Close()
	if err != nil {
		return err
	}
	json, err := task.Marshal()
	if err != nil {
		return err
	}
	_, err = file.Write(json)
	if err != nil {
		return err
	}
	manager.tasks[task.Name] = task
	return nil
}

func (manager *taskManager) Load() error {
	// Write Lock
	manager.Lock()
	log.Debug("TASKS WRITE LOCK (+)")
	defer log.Debug("TASKS WRITE LOCK (-)")
	defer manager.Unlock()
	//
	log.Info("LOADING SAVED TASKS")
	return filepath.Walk(
		goofy.TasksDirectory,
		func(path string, info os.FileInfo, err error) error {
			switch {
			case info.IsDir():
				log.Trace("ENTERING DIRECTORY (%s)", path)
			case err != nil:
				log.Error("ERROR DURING WALK TO %s > %s", path, err)
			default:
				task := new(goofy.Task)
				json, err := ioutil.ReadFile(path)
				if err != nil {
					log.Error("CANNOT READ FILE > %s", path)
					return err
				}
				err = task.Unmarshal(json)
				if err != nil {
					log.Error("CANNOT UNMARSHAL JSON TASK FROM FILE > %s", path)
					return err
				}
				manager.tasks[task.Name] = task
				log.Debug("TASK ADDED > %s", task.Name)
			}
			return nil
		},
	)
}

func (_ *taskManager) Follow(client net.Conn) {
	//TODO: Allow to follow a task based on start time or maybe a run-ID
	//Currently, it will follow the first listed log for a given task on a given worker
	//Read goofy.FollowRequest
	data := make([]byte, 1024)
	n, err := client.Read(data)
	data = data[:n]

	log.Debug(string(data))
	var request goofy.FollowRequest
	err = json.Unmarshal(data, &request)
	check(err)

	wrkDirectory := strconv.Itoa(int(request.WorkerID)) + "/" + request.Taskname + "/"
	files := make([]os.FileInfo, 0)
	for len(files) <= 0 { //while no file found OR task not ended
		files, err = ioutil.ReadDir(wrkDirectory)
		if err != nil {
			log.Fatal("FATAL > %s", err)
			client.Close()
			return
		}
	}
	filename := files[len(files)-1].Name()

	worker, err := Worker.Get(request.WorkerID)
	if err != nil {
		log.Error("%s", err)
	}
	conn, err := Security.DialTLS(
		net.JoinHostPort(worker[0].IP, "1338"),
	)
	if err != nil {
		log.Error("%s", err)
	}
	workerClient := rpc.NewClient(conn)

	t, err := tail.TailFile(wrkDirectory+filename, tail.Config{Follow: true})
	go func() {
		for line := range t.Lines {
			client.Write([]byte(line.Text + "\n"))
		}
	}()

	taskComplete := false
	for !taskComplete {
		err := workerClient.Call("Task.IsTaskComplete", request.Taskname, &taskComplete)
		if err != nil {
			log.Error("%s", err)
		}
	}

	log.Trace("Task completed, transmission to client done.")

	t.Stop()
	client.Close()
}

func check(err error) {
	if err != nil {
		log.Error("%s", err)
	}
}
