/*
Copyright 2016 George Trudeau

This file is part of goofy.

goofy is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

goofy is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with goofy. If not, see <http://www.gnu.org/licenses/>.
*/

package main

import (
	"goofy"
	"goofy/log"
	"os"
	"path/filepath"
)

var version string

func init() {
	os.Chdir(
		filepath.Dir(
			os.Args[0],
		),
	)
}

func main() {
	line := &goofy.CommandLine{
		usage,
		goofy.Commands{
			"start": {
				Call:  start,
				Flags: startFlags(),
				Usage: startUsage,
			},
		}}
	command := line.Parse()
	verbosity := log.TRACE
	command.Flags.Var(
		&verbosity,
		"verbosity",
		"{OFF,FATAL,ERROR,WARN,INFO,DEBUG,TRACE}",
	)
	command.Parse()
	log.Verbosity(verbosity)
	command.Call()
}
