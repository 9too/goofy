# Build configurations
GOPATH  := $(PWD)
VERSION := 0.0.1

BUILD_DIR := build
SRC_DIR   := src
SRC_GOOFY := $(SRC_DIR)/goofy

CA          := CA
SERVER_CERT := $(BUILD_DIR)/server/certificate
CERTIFICATE := $(BUILD_DIR)/%/certificate

WEB := $(BUILD_DIR)/server/web

PACKAGES  := client server worker

DEPENDENCIES = $(WEBSOCKET) $(TAIL)
WEBSOCKET := golang.org/x/net/websocket
TAIL      := github.com/hpcloud/tail

BUILD  = $(FLAGS) -o $(OUTPUT) $(TARGET)
OUTPUT = $@/goofy-$(*F)
TARGET = goofy/$(*F)

.PHONY: default clean all debug release $(PACKAGES) $(WEB)

all: $(PACKAGES)
clean:
	@rm -rf $(BUILD_DIR) pkg/
dev:
	make clean && make -j6 && sh script/run.sh

# General builds
debug: .debug-flags all
release: .release-flags all

.release-flags:
	$(eval FLAGS = -ldflags "-w -X main.version=$(VERSION)")
.debug-flags:
	$(eval FLAGS = -gcflags "-N -l")

# Packages
server: $(WEBSOCKET) $(TAIL) \
		$(BUILD_DIR)/server $(CA) $(WEB)
worker: $(BUILD_DIR)/worker server
client: $(BUILD_DIR)/client server

# Build template
$(BUILD_DIR)/%: $(SRC_GOOFY)/% | $(CERTIFICATE)
	go build $(BUILD)

$(DEPENDENCIES):
	go get $@

$(WEB):
	@mkdir -p $@
	@cp -ru web/static/* $@
	go run script/compile_html.go > $@/index.html

# Certificates
.SECONDARY:
$(CERTIFICATE): $(CA)
	@mkdir -p $@
	@cp $</cert.pem $@/ca.pem
$(SERVER_CERT): $(CA)
	@mkdir -p $@
	@cp $</*.pem $@
$(CA):
	@mkdir -p $@
	go run script/generate_ca.go -o $@
