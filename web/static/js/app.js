// RPC WebSocket
var rpc;
// Callback handlers for RPC WebSocket
var handlers = new Map([
        for(handler of [GetTasks, GetWorkers, NewTask, FollowTask, ListTasks, ListWorkers])
            [handler.name, handler]
]);
// Initialize window
if ('WebSocket' in window) {
    Connect()
} else {
    alert('WebSocket is not supported by your browser');
}

/*
 * Websocket Initialization
 */

function Connect() {
    rpc = new WebSocket('wss://'+window.location.host+'/ws');
    rpc.onopen = function() {
        console.log('opening websocket');
        // TODO : Send credentials or token
        GetTasks();
        GetWorkers();
    };
    rpc.onmessage = function(response) {
        var response = JSON.parse(response.data);
        if (response.error != null) {
            error(response.error);
        } else {
            console.log(response.id, response.result);
            handlers.get(response.id)(response.result);
        }
    };
    rpc.onclose = function() {
        console.log('websocket closed');
    };
}

function ConnectOutput(workerID, taskname) {
    outws = new WebSocket("wss://"+window.location.host+"/out");
    outws.onopen = function() {
        console.log("opening websocket");
        outws.send('{"WorkerID":' + workerID + ',"Taskname":"' + taskname+'"}');

        // TODO : Send credentials or token
    };
    outws.onmessage = function (e) {
        var response = e.data;
        console.log(response);
    };
    outws.onclose = function() {
        console.log("websocket closed");
    };
}

/*
 * JSON RPC
 */

function RPC(callback, method, params) {
    rpc.send( JSON.stringify( {
        id:     callback.name,
        method: method,
        params: (params == null ? [] : [params])
    }));
}

function GetTasks() {
    RPC(ListTasks,'Task.List')
}

function GetWorkers() {
    RPC(ListWorkers, 'Worker.List')
}

/*
 * Callback Handlers
 */

function ListTasks(tasks) {
    var list = document.getElementById('task-list');
    while (list.firstChild) {
        list.removeChild(list.firstChild);
    }
    if (tasks != null) {
        tasks.sort(
            function(a,b) {
                if(a.Name < b.Name) return -1;
                if(a.Name > b.Name) return 1;
                return 0;
        });
        var i = 1;
        for (var task of tasks) {
            var id       = 'task-' + i++;
            var mod      = 'modify-' + id;
            var template = document.getElementById('task-template').content;
            var labels   = template.querySelectorAll('label');
            var show     = template.querySelectorAll('p.show');
            labels[0].htmlFor = id;
            labels[1].htmlFor = mod;
            labels[3].htmlFor = mod;
            show[0].textContent = task.Description;
            show[1].textContent = task.Command;
            template.querySelector('input.expand').id = id;
            template.querySelector('input.edit').id = mod;
            template.querySelector('input[name=name]').value = task.Name;
            template.querySelector('h2').textContent = task.Name;
            list.appendChild(document.importNode(template, true));
        }
    }
}

function NewTask() {
    document.getElementById('new-task').reset();
    document.getElementById('hide-new-task').click();
    GetTasks();
}

function FollowTask() {

}

function ListWorkers(workers) {
    var list = document.getElementById('worker-list');
    while (list.firstChild) {
        list.removeChild(list.firstChild);
    }
    list.size = list.length;
    if (workers != null) {
        for (var worker of workers) {
            var template = document.getElementById('worker-template').content;
            template.querySelector('option').value = worker.ID;
            info = template.querySelectorAll('span');
            info[0].textContent = worker.ID;
            info[1].textContent = worker.Hostname;
            list.appendChild(document.importNode(template, true));
        }
    }
}

/*
 * Form Functions
 */

function ConfirmDeleteTask(button) {
    var modal = document.getElementById('task-modal');
    var task = button.parentNode.querySelector('input[name=name]').value;
    modal.hidden = false;
    modal.querySelector('h4').textContent = task;
    modal.querySelector('p').textContent  = 'Delete this task ?';
    window.onclick = function(event) {
        if (event.target == modal) {
            modal.hidden = true;
        }
    }
}

function DeleteTask(button) {
    var name = button.parentNode.querySelector('h4').textContent;
    RPC(GetTasks, 'Task.Delete', name);
    button.closest('.modal').hidden = true;
}

function EditTask(button) {
    map(
        button.closest('.task').querySelectorAll('.task-detail'),
        function(detail) {
            detail.querySelector('textarea').value = detail.querySelector('p').textContent;
    });
}

function ModifyTask(button) {
    var form = button.form.elements;
    RPC(GetTasks, 'Task.Modify', {
        Name:        form['name'].value,
        Description: form['description'].value,
        Command:     form['command'].value,
    });
}

function RunTask(button) {
    var workers = document.querySelectorAll('#workers :checked');
    if (workers.length == 0) {
        error('No workers selected');
        return;
    }

    var taskname = button.parentNode.querySelector('input[name=name]').value;
        RPC(FollowTask, 'Task.Dispatch', {
        Workers: [for (worker of workers) parseInt(worker.value)],
        Task: taskname,
    });

    for(var i=0; i < workers.length; i++) {
        ConnectOutput(parseInt(workers[i].value), taskname);
    }
}



function SubmitTask(button) {
    var form = button.form.elements;
    RPC(NewTask, 'Task.New', {
        Name:        form['name'].value,
        Description: form['description'].value,
        Command:     form['command'].value,
    });
}

/*
 * General Functions
 */

function error(message) {
    console.error(message);
    document.getElementById('error-msg').textContent = message;
    document.getElementById('error').checked = true;
}

function closeModal(button) {
    button.closest('.modal').hidden = true;
}

function check(element) {
    element.checked = true;
}

function uncheck(element) {
    element.checked = false;
}

function collapse(selector) {
    map(document.querySelectorAll(selector + ' .collapse'), uncheck);
}

function expand(selector) {
    map(document.querySelectorAll(selector + ' .expand'), check);
}

function map(array, f) {
    for (var i = 0, len = array.length; i < len; i++) {
        f(array[i]);
    }
}
