# goofy

goofy is a simplistic automation platform - as it's name suggests.

It aims at leveraging the orchestration of different tasks while providing an entirely free playground.
It will adapt to *YOU*, not the other way around. It leaves you in charge of scripting your task in the language you like, then you configure the task so goofy can take care of the rest.

## Introduction

goofy is a distributed platform to run tasks across a network. 
You can either have a complete setup on a single computer or have every components run on different ones.
It will allow you to automate tests, triggers or whatever you wish to automate.

## Architecture

* **goofy-server**

  The server is the central piece that organizes everything.
  It receives requests from the clients and distribute tasks to the workers.

* **goofy-cli**

  The cli is a command line interface that provides client access to the server

* **goofy-webapp**

  The webapp is an HTTP server that provides a client web interface to the server.

* **goofy-worker**

  The workers are the components that run tasks and reports their execution.
  Multiple workers can be registered on the server.

## Building the project

*Prerequisites*
- Go
- GNU Make

### Quick localhost setup

In a terminal:
```bash
$ cd path/to/goofy
$ make
$ sh script/run.sh # Requires color terminal
```
In an another terminal:
```bash
$ cd path/to/goofy/build/cli
$ goofy-cli help
```

## Security

All communications between goofy actors are exchanged on top of TLS.
Meaning you can safely communicate sensitive internal informations directly out-of-the-box, such as passwords and whatnot.

### Registration process

When you build the project, a fresh new certificate will be generated for the server.
That certificate is a self-signed CA (Certificate Authority) and the server's identity certificate.
The CA will be packaged under `certificate/ca.pem` for every other goofy components, to allow them to safely initiates communication with the server.

On top of this, there is a registration process for every worker that you wish to link.
The process only requires you to aknowledge the worker's request (NOT YET IMPLEMENTED) in the server.

1. The worker sends a CSR (Certificate Signing Request) to the server when started with "-new"
2. **You accept the worker registration**
3. The server signs and registers the worker's certificate
4. The worker receives it's CA-signed certificate

From now on, both the server and worker can authenticate each other and operate over TLS.
