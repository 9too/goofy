#!/bin/bash

COLOR_LOG=$'
s/(===)/\e[90m\\1\e[m/;
s/(INFO|DEBUG|TRACE)/\e[32m\\1\e[m/;
s/(WARN)/\e[33m\\1\e[m/;
s/(ERROR|FATAL)/\e[31m\\1\e[m/
'

cd "$(dirname "$0")/../build/"

server/goofy-server start      | sed -E $'s/.*/\e[35mSERVER: \e[m&/;'"$COLOR_LOG" &
worker/goofy-worker start -new | sed -E $'s/.*/\e[36mWORKER: \e[m&/;'"$COLOR_LOG" &

sleep infinity
