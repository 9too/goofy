/*
Copyright 2016 George Trudeau

This file is part of goofy.

goofy is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

goofy is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with goofy. If not, see <http://www.gnu.org/licenses/>.
*/

// This file is inspired from crypto/tls/generate_cert.go

// +build ignore

package main

import (
	"crypto/rand"
	"crypto/rsa"
	"crypto/x509"
	"crypto/x509/pkix"
	"encoding/pem"
	"flag"
	"fmt"
	"io/ioutil"
	"math/big"
	"net"
	"os"
	"path/filepath"
	"strings"
	"time"
)

var (
	keysize = flag.Int(
		"keysize",
		2048,
		"RSA key size",
	)
	host = flag.String(
		"host",
		"127.0.0.1",
		"Comma-separated hostnames and IPs to generate a certificate for",
	)
	duration = flag.Duration(
		"duration",
		365*24*time.Hour,
		"Duration that certificate is valid for",
	)
	out = flag.String(
		"o",
		".",
		"Path to output generated PEM files",
	)
)

func main() {
	flag.Parse()
	key, err := rsa.GenerateKey(rand.Reader, *keysize)
	if err != nil {
		fail("generate key", err)
	}
	notBefore := time.Now()
	notAfter := notBefore.Add(*duration)
	template := x509.Certificate{
		Subject: pkix.Name{
			Organization: []string{"goofy"},
		},
		SerialNumber:          big.NewInt(0),
		NotBefore:             notBefore,
		NotAfter:              notAfter,
		IsCA:                  true,
		KeyUsage:              x509.KeyUsageCertSign | x509.KeyUsageDigitalSignature | x509.KeyUsageKeyEncipherment,
		BasicConstraintsValid: true,
	}
	hosts := strings.Split(*host, ",")
	for _, h := range hosts {
		if ip := net.ParseIP(h); ip != nil {
			template.IPAddresses = append(template.IPAddresses, ip)
		} else {
			template.DNSNames = append(template.DNSNames, h)
		}
	}
	derBytes, err := x509.CreateCertificate(
		rand.Reader,
		&template, &template,
		key.Public(), key,
	)
	if err != nil {
		fail("create certificate", err)
	}
	certOut, err := os.Create(
		filepath.Join(*out, "cert.pem"),
	)
	if err != nil {
		fail("open cert.pem for writing", err)
	}
	pem.Encode(
		certOut,
		&pem.Block{
			Type:  "CERTIFICATE",
			Bytes: derBytes,
		},
	)
	certOut.Close()
	keyOut, err := os.OpenFile(
		filepath.Join(*out, "key.pem"),
		os.O_WRONLY|os.O_CREATE|os.O_TRUNC,
		0600,
	)
	if err != nil {
		fail("open key.pem for writing:", err)
	}
	pem.Encode(
		keyOut,
		&pem.Block{
			Type:  "RSA PRIVATE KEY",
			Bytes: x509.MarshalPKCS1PrivateKey(key),
		},
	)
	keyOut.Close()
}

func fail(msg string, err error) {
	fmt.Fprintf(os.Stderr, "FAILED : %s > %s\n", msg, err)
	os.Exit(2)
}

func loadCA(caPath, keyPath *string) (*x509.Certificate, *rsa.PrivateKey) {
	caPEM, err := ioutil.ReadFile(*caPath)
	if err != nil {
		fail("read CA certificate", err)
	}
	caBlock, _ := pem.Decode(caPEM)
	caCert, err := x509.ParseCertificate(caBlock.Bytes)
	if err != nil {
		fail("parse CA certificate", err)
	}
	keyPEM, err := ioutil.ReadFile(*keyPath)
	if err != nil {
		fail("read CA key", err)
	}
	keyBlock, _ := pem.Decode(keyPEM)
	caKey, err := x509.ParsePKCS1PrivateKey(keyBlock.Bytes)
	if err != nil {
		fail("parse CA key", err)
	}
	return caCert, caKey
}
