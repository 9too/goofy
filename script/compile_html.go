/*
Copyright 2016 George Trudeau

This file is part of goofy.

goofy is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

goofy is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with goofy. If not, see <http://www.gnu.org/licenses/>.
*/

// +build ignore

package main

import (
	"html/template"
	"io/ioutil"
	"os"
)

/*
 * Placeholders
 */

type Property struct {
	Title string
}

type Layout struct {
	Header,
	Workers,
	Footer template.HTML
}

type Content struct {
	Home,
	Tasks template.HTML
}

type Placeholders struct {
	Property
	Layout
	Content
}

/*
 * Compile
 */

func main() {

	/*
	 * Layout
	 */
	header, err := ioutil.ReadFile("web/template/content/header.html")
	check(err)
	workers, err := ioutil.ReadFile("web/template/content/workers.html")
	check(err)
	footer, err := ioutil.ReadFile("web/template/content/footer.html")
	check(err)

	/*
	 * Contents
	 */
	home, err := ioutil.ReadFile("web/template/content/main/home.html")
	check(err)
	tasks, err := ioutil.ReadFile("web/template/content/main/tasks.html")
	check(err)

	/*
	 * Template
	 */
	template.Must(
		template.ParseFiles("web/template/layout.tmpl"),
	).Execute(
		os.Stdout,
		Placeholders{
			Property{
				Title: "GOOFY",
			},
			Layout{
				Header:  template.HTML(header),
				Workers: template.HTML(workers),
				Footer:  template.HTML(footer),
			},
			Content{
				Home:  template.HTML(home),
				Tasks: template.HTML(tasks),
			},
		},
	)
}

func check(err error) {
	if err != nil {
		panic(err)
	}
}
